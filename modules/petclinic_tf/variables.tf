# Defining variables

variable "region" {
  description = "AWS Deployment region.."
  default = "eu-west-1"
}

variable "environment_tag" {
    description = "group name tag"
    default = "bt"
}

variable "public_key_path" {
    description = "This is the path to the ssh key"
    default = "~/.ssh/authorized_keys"
}

variable "db_name" {
    description = "Name of the RDS DB"
    default = "petclinic_db"
}

variable "db_user" {
    description = "Username of the RDS DB"
    default = "admin"
}

variable "db_password" {
    description = "Password of the RDS DB"
    default = "secret!123"
}

variable "vpc_id" {

}

# variable "subnet_id"{
    
# }

variable "private_subnet_id_1a" {

}

variable "private_subnet_id_1b" {

}

variable "private_subnet_id_1c" {

}

# variable "sg_web_id" {

# }

variable "sg_jenkins_master_id" {

}

variable "sg_jenkins_worker_id" {

}
variable "base_ami" {
    description = "Amazon Linux AMI"
    default = "ami-063d4ab14480ac177"
}

variable "instance_type" {
    description = "Default instance type"
    default = "t2.micro"
}

variable "zone_A"{
    description = "Availablility zone A"
    default = "eu-west-1a"
}

variable "zone_B"{
    description = "Availablility zone b"
    default = "eu-west-1b"
}

# variable "subnet_id_A"{

# }

# variable "subnet_id_B"{

# }

# variable "sg_lb_id"{

# }

variable "public_subnet_1a_id" {
  
}

variable "public_subnet_1b_id" {
  
}

variable "public_subnet_1c_id" {
  
}