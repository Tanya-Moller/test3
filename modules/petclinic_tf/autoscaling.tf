## Webserver SG
resource "aws_security_group" "sg_web" {
  name = "sg_${var.environment_tag}_webserver"
  vpc_id = var.vpc_id
  description = "Allow 8080 from lb and SSH from Jenkins"
  ingress {
      from_port   = 80
      to_port     = 80      #These should be 8080
      protocol    = "tcp"
      security_groups = [aws_security_group.sg_lb.id]
  }
  ingress {
      from_port   = 22
      to_port     = 22
      protocol    = "tcp"
      security_groups = [var.sg_jenkins_master_id, var.sg_jenkins_worker_id, aws_security_group.sg_lb.id]
  }
  egress {
      from_port   = 0
      to_port     = 0
      protocol    = "-1"
      cidr_blocks = ["0.0.0.0/0"]
    }
  tags = {
      "Environment" = "${var.environment_tag}"
      "Name"        = "sg-${var.environment_tag}-webserver"
    }
}

# resource "aws_launch_configuration" "default" {
#   image_id        = "ami-063d4ab14480ac177"
#   instance_type   = "t2.micro"
#   security_groups = [aws_security_group.sg_web.id]
#   user_data = <<-EOF
#             #!/bin/bash
#             sudo yum -y install httpd
#             sudo systemctl start httpd
#             sudo sh -c "cat >/var/www/html/index.html <<_END_
#             <h1> OMG WTF</h1>
#             <img src=\"https://i.pinimg.com/originals/5d/50/96/5d50964c25f8ce13e5531c60892c03dd.gif\">
#             _END_"
#             '
#               EOF
#   lifecycle {
#     create_before_destroy = true
#   }
# }

resource "aws_launch_template" "default" {
  name = "bt_webserver_lt"
  instance_type = "t2.micro"
  image_id        = "ami-063d4ab14480ac177"
  vpc_security_group_ids = [aws_security_group.sg_web.id]
  tags = {
      Name = "webserver"
    }
  # user_data = <<-EOF
  #           #!/bin/bash
  #           sudo yum -y install httpd
  #           sudo systemctl start httpd
  #           sudo sh -c "cat >/var/www/html/index.html <<_END_
  #           <h1> OMG WTF</h1>
  #           <img src=\"https://i.pinimg.com/originals/5d/50/96/5d50964c25f8ce13e5531c60892c03dd.gif\">
  #           _END_"
  #           '
  #             EOF

}

data "aws_availability_zones" "all" {}

resource "aws_autoscaling_group" "default" {
  # availability_zones   = data.aws_availability_zones.all.names
  vpc_zone_identifier = [ var.public_subnet_1a_id, var.public_subnet_1b_id, var.public_subnet_1c_id ]

  min_size = 2
  max_size = 10

  target_group_arns = [ aws_lb_target_group.default.arn ]  #maybe this should be id??
  health_check_type = "ELB"

  launch_template {
    id      = aws_launch_template.default.id
    version = "$Latest"
  }
  lifecycle {
    ignore_changes = [load_balancers, target_group_arns]
  }

  # The "ELB" health check is much more robust, as it tells the ASG to use the CLB’s health check to determine if an Instance is healthy or not and to automatically replace Instances if the CLB reports them as unhealthy. That way, Instances will be replaced not only if they are completely down, but also if, for example, they’ve stopped serving requests because they ran out of memory or a critical process crashed.
  tag {
    key                 = "Name"
    value               = "bt_asg"
    propagate_at_launch = true
  }
} 

resource "aws_lb" "default" {
  name               = "bt-loadbalancer"
  internal           = false
  load_balancer_type = "application"
  # availability_zones = data.aws_availability_zones.all.names
  security_groups = [ aws_security_group.sg_lb.id ]
  subnets = [ var.public_subnet_1a_id, var.public_subnet_1b_id, var.public_subnet_1c_id ]

  tags = {
      Name = "bt_lb"
    }
}

#target group
resource "aws_lb_target_group" "default" {
  name     = "bt-lb-tg"
  port     = 8080
  protocol = "HTTP"
  vpc_id   = var.vpc_id
    health_check {
    healthy_threshold   = 2
    unhealthy_threshold = 2
    timeout             = 3
    interval            = 30
    }
}

#listener
resource "aws_lb_listener" "default" {
  load_balancer_arn = aws_lb.default.arn
  port              = "8080"
  protocol          = "HTTP"

  default_action {
    type = "redirect"

    redirect {
      port        = "80"
      protocol    = "HTTP"
      status_code = "HTTP_301"
    }
  }
  }

resource "aws_autoscaling_attachment" "asg_attachment_bt" {
  autoscaling_group_name = aws_autoscaling_group.default.id
  alb_target_group_arn   = aws_lb_target_group.default.arn
}

## Loadbalancer SG
resource "aws_security_group" "sg_lb" {
  name = "sg_${var.environment_tag}_loadbalancer"
  vpc_id = var.vpc_id
  description = "Allow access from all and SSH from Jenkins"
  ingress {
      from_port   = 8080 #  This should be port 8080
      to_port     = 8080
      protocol    = "tcp"
      cidr_blocks = ["0.0.0.0/0"]
      ipv6_cidr_blocks = ["::/0"]
  }
  ingress {
      from_port   = 22
      to_port     = 22
      protocol    = "tcp"
      security_groups = [var.sg_jenkins_master_id, var.sg_jenkins_worker_id]
  }
egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
tags = {
    "Environment" = "${var.environment_tag}"
    "Name"        = "sg-${var.environment_tag}-loadbalancer"
  }
}

output "clb_dns_name" {
  value       = aws_lb.default.dns_name
  description = "The domain name of the load balancer"
}