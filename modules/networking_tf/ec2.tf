
## Jenkins SG
resource "aws_security_group" "sg_jenkins_master" {
  name = "sg_${var.environment_tag}_jenkins_master"
  vpc_id = "${aws_vpc.vpc.id}"
  description = "Allow access from home IPs"
  ingress {
      from_port   = 8080
      to_port     = 8080
      protocol    = "tcp"
      cidr_blocks = ["90.212.182.216/32", "82.33.58.195/32"]
  }
egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
tags = {
    "Environment" = "${var.environment_tag}"
    "Name"        = "sg-${var.environment_tag}-jenkins_master"
  }
}

resource "aws_security_group" "sg_jenkins_worker" {
  name = "sg_${var.environment_tag}_jenkins_worker"
  vpc_id = "${aws_vpc.vpc.id}"
  description = "Allow access from home IPs and Jenkins IPs"
  ingress {
      from_port   = 8080
      to_port     = 8080
      protocol    = "tcp"
      cidr_blocks = ["90.212.182.216/32", "82.33.58.195/32"]
  }
  ingress {
      from_port   = 0
      to_port     = 0
      protocol    = "-1"
      security_groups = [aws_security_group.sg_jenkins_master.id] #### Worker needs master IP
  }
egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
tags = {
    "Environment" = "${var.environment_tag}"
    "Name"        = "sg-${var.environment_tag}-jenkins_worker"
  }
}


resource "aws_instance" "jenkins_master" {
  ami = "ami-0224148196d608df4"
  instance_type = "t2.micro"
  availability_zone = "eu-west-1a"
  associate_public_ip_address = true
  subnet_id = aws_subnet.subnet_public_1a.id
  security_groups = [aws_security_group.sg_jenkins_master.id]
  key_name = "BTKEY"
  tags = {
      "Name" = "bt-jenkins-master"
  }
}

resource "aws_instance" "jenkins_worker" {
  ami = "ami-06452ff11523e2f53"
  count = 2
  instance_type = "t2.micro"
  availability_zone = "eu-west-1a"
  associate_public_ip_address = true
  subnet_id = aws_subnet.subnet_public_1a.id
  security_groups = [aws_security_group.sg_jenkins_worker.id]
  iam_instance_profile = "ALAcademyJenkinsSuperRole"
  key_name = "BTKEY"
  tags = {
      "Name" = "bt-jenkins-worker-${count.index}"
  }
}